{-# LANGUAGE TypeApplications #-}

module Types where

import Data.Int (Int32)

type Pulse = [Float]
type Seconds = Float
type Samples = Float
type Hz = Float
type Semitones = Float
type Beats = Float
type Ringtone = String

data Tone = C | CSharp | D | DSharp | E | F | FSharp | G | GSharp | A | ASharp | B deriving (Enum, Eq, Show)
data Octave = Zero | One | Two | Three | Four | Five | Six | Seven | Eight deriving (Enum, Eq, Show)
data Duration = Full | Half | Quarter | Eighth | Sixteenth | Thirtysecond | Dotted Duration deriving (Eq, Show)
data Note = Pause Duration | Note Tone Octave Duration deriving (Eq, Show)

-- TODO: Schrijf en documenteer zipWithL, die zo ver mogelijk zipt;
-- als na het zippen, nog waarden in de eerste lijst zitten, plakt het deze er achteraan;
-- als na het zippen, nog waarden in de tweede lijst zitten, worden die weggegooid.
{-|
Deze functie pakt het eerste element van lijst 1 en het eerste element van lijst 2 waar een
waarde uit komt. deze waarde wordt in een nieuwe lijst gezet. Dit gaat door totdat één van de lijsten 
geen waardes meer heeft
als één van de lijsten leeg is en in de eerste lijst nog een waarde zit word deze waarde erachter geplakt,
anders worden alle waarden weggegooid.
en de lijste met opgetelde waarde terug gegeven 
-}

zipWithL :: (a -> b -> a) -> [a] -> [b] -> [a]
zipWithL f (x:xs) (y:ys) = (f x y) : (zipWithL f xs ys)
zipWithL f [] [] = []
zipWithL f xs [] = (zipWithL f [] []) ++ xs
zipWithL f [] ys = []

-- TODO: Schrijf en documenteer zipWithR, die zo ver mogelijk zipt;
-- als na het zippen, nog waarden in de eerste lijst zitten, worden die weggegooid;
-- als na het zippen, nog waarden in de tweede lijst zitten, plakt het deze er achteraan.
{-|
Deze functie pakt het eerste element van lijst 1 en het eerste element van lijst 2 waar een
waarde uit komt. deze waarde wordt in een nieuwe lijst gezet. Dit gaat door totdat één van de lijsten 
geen waardes meer heeft
als één van de lijsten leeg is en in de tweede lijst nog een waarde zit word deze waarde erachter geplakt,
anders worden alle waarden weggegooid.
en de lijste met opgetelde waarde terug gegeven 
-}
zipWithR :: (a -> b -> b) -> [a] -> [b] -> [b]
zipWithR f (x:xs) (y:ys) = (f x y) : (zipWithR f xs ys)
zipWithR f [] [] = []
zipWithR f [] ys = (zipWithR f [] []) ++ ys
zipWithR f xs [] = []

data Sound = FloatFrames [Float]
  deriving Show

floatSound :: [Float] -> Sound
floatSound = FloatFrames

instance Eq Sound where
  (FloatFrames xs) == (FloatFrames ys) = (all ((<  0.001) . abs) $ zipWith (-) xs ys) && (length xs == length ys)

-- TODO: Schrijf de instance-declaraties van Semigroup en Monoid voor Sound.
-- Semigroup is een typeclass met een operator (<>), die twee waarden combineert;
-- in deze context betekent dat "twee geluiden na elkaar afspelen".
-- Monoid bouwt voort op Semigroup, maar heeft een mempty; een lege waarde.
{-|
Deze instance zorgt ervoor dat de lijsten 'a' en 'b' zolang ze van het type [float] zijn, gecombineerd worden en dan naar Sound.
-}
instance Semigroup Sound where
  (FloatFrames a) <> (FloatFrames b) = floatSound (a++b)  

{-|
Deze instance definiëert mempty als een lege sound.
-}
instance Monoid Sound where
  mempty = floatSound []

-- TODO: Schrijf en documenteer de operator `(<+>)` die twee `Sound`s  tot een enkel `Sound` combineert.
-- Combineren betekent hier: de geluiden tegelijkertijd afspelen. 
-- Als de lijsten niet even lang zijn, moet wat er overblijft achteraan worden toegevoegd!
{-|
Deze operator kijkt eerst naar de lengtes van beide FloatFrames x en y. Als de eerste langer is dan de tweede wordt
de functie zipWithL toegepast. Als de tweede langer is dan de eerste wordt de functie zipWithR toegepast.
-}
(<+>) :: Sound -> Sound -> Sound
(FloatFrames x) <+> (FloatFrames y) = if length x >= length y
                                        then floatSound (zipWithL (+) x y) 
                                        else floatSound (zipWithR (+) x y) 

floatToInt32 :: Float -> Int32
floatToInt32 x = fromIntegral $ round x

getAsInts :: Sound -> [Int32]
getAsInts (FloatFrames fs) = map (floatToInt32 . \x -> x * fromIntegral (div (maxBound @Int32 ) 2 )) fs

type Track = (Instrument, [Note])

newtype Instrument = Instrument (Hz -> Seconds -> Pulse)

instrument :: (Hz -> Seconds -> Pulse) -> Instrument
instrument = Instrument

newtype Modifier = Modifier (Pulse -> Pulse)

modifier :: (Pulse -> Pulse) -> Modifier
modifier = Modifier

instance Semigroup Modifier where
  (Modifier m1) <> (Modifier m2) = Modifier $ m1 . m2

-- TODO: Schrijf en documenteer de functie modifyInstrument, die een Modifier met een Instrument combineert. 
-- TIPS: Kijk goed naar de types! Gebruik een lambda om een functie te maken, die je verpakt in een Instrument.
{-|
zet een intrument om naar modifier en weer terug naar instrument 
de functie pakt de hz en seconds van een instrument en geeft deze mee aan "i" wat het omzet naar een pulse 
deze pulse word doorgegven aan m wat er een modefier van maakt (wat ook een pulse is)
deze modifier pulse wordt als een pulse van instrument terug gegeven 
-}
modifyInstrument :: Instrument -> Modifier -> Instrument
modifyInstrument (Instrument i) (Modifier m) = instrument(\x y -> m(i x y)) 

-- TODO: Schrijf en documenteer de functie arrange die de functie in het meegegeven Instrument toepast op de frequentie en duur. 
-- TIPS: Kijk goed naar de types!
{-|
zet een instrument om naar een sound 
pakt de instrument functie en geeft deze hz en seconds mee
hoer komt een pulse uit en deze pulse wordt omgezet naar een sound(floatSound)
-}
arrange :: Instrument -> Hz -> Seconds -> Sound
arrange (Instrument i) x y = floatSound (i x y)
