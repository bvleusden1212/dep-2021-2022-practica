module Parsers where

import Control.Monad.Trans.State (StateT(..), evalStateT, put, get)
import Control.Applicative
import Control.Monad.Trans.State (StateT(..), evalStateT, put, get)
import Data.Maybe (isJust, fromMaybe)
import Data.Char (toUpper)
import Control.Monad(mzero, mplus)
import Data.List (uncons)
import Types
import Instruments

type Parser = (StateT String Maybe)
type CharSet = [Char]

pCharSet :: CharSet -> Parser Char
pCharSet cs = do input <- uncons <$> get
                 case input of
                   Just (h, t) -> if h `elem` cs then put t >> return h else mzero
                   Nothing -> mzero

-- TODO: Schrijf en documenteer de Parser `pComplementCharSet` die een lijst van karakters meekrijgt,
-- en het eerste karakter parset wanneer dit niet in de meegegeven set karakters zit.
{-|
check of een string geldig is of niet 
we gebruiken de paser van de functie PCharSet en veranderen de satement na de "then"
we veranderen de statement zo dat de statement zegt als h in de lijst met karakters zit dan geeft de code niks terug 
als h wel in de lijst met karakter zit geeft de code de volgende itteratie(t) van de parser en het karakter(h) dat is goedgekeurt
als een type parser met als waarde Char
-}
pComplementCharSet :: CharSet -> Parser Char
pComplementCharSet xs = do input <- uncons <$> get
                           case input of
                             Just (h, t) -> if h `elem` xs then mzero else put t >> return h
                             Nothing -> mzero

-- TODO: Schrijf en documenteer de Parser `pString` die een gegeven String probeert te parsen. 
-- TIPS: Parse een enkele letter met `pCharSet` en parse de rest recursief met `pString`; 
--       combineer beide waarden weer voordat je deze returnt. 
--       Vergeet niet een geval voor een lege String te schrijven.
{-|
parset een string 
eerst pakt de code de eerste letter van de gehele string 
met een monad(do statement) pakken we de eerste letter als input en defineren de functie met de rest van de string als parser "p"
we returnen p met daaraan toegevoegd de input in een lijst 
dit gaat door totdat de string leeg is en geven dan de geparste string terug 
-}
pString :: String -> Parser String
pString [] = return []
pString (s:str) = do input <- pCharSet [s]
                     p <- pString str
                     return (input : p)
                      
                     

pOptional :: Parser a -> Parser (Maybe a)
pOptional p = Just <$> p <|> return Nothing 

pRepeatSepBy :: Parser a -> Parser b -> Parser [b]
pRepeatSepBy sep p = (:) <$> p <*> mplus (sep *> pRepeatSepBy sep p) (return [])

pEmpty :: Parser ()
pEmpty = return ()

-- TODO: Schrijf en documenteer de Parser `pRepeat` die een enkele Parser herhaaldelijk toepast.
-- TIPS: Maak gebruik van `pRepeatSepBy` en `pEmpty`.
{-|
voert de meegegeven parser herhaaldelijk toe 
door de al gemaakte functie "pReapeatSepBy" de gberuiken kunnnen we een parser herhaaldelijk uitvoeren 
de al gemaakte functie "pEmpty" zetten we neer als plase holder voor de functie maar wordt overschreven als we een daarwerkelijke 
parser terug geven
-}
pRepeat :: Parser a -> Parser [a]
pRepeat = pRepeatSepBy pEmpty

-- TODO: Schrijf en documenteer de Parser `pNumber`, die een geheel getal parset.
-- TIPS: Combineer de voorgaande Parsers en de Prelude-functie read. 
{-|
parset een geheel getal uit een willekeurige string met int en string 
met "pRepeat(pCharSet "0123456789")" maken we een parse van alle gehele getallen die we kunnen gebruiken
met "<$>" passen we "read" to op alle karakters wat en read haalt alle intergers er uit, en geeft de een int terug.  
-}
pNumber :: Parser Int
pNumber = read <$> pRepeat(pCharSet "0123456789")
               

pTone :: Parser Tone
pTone = do tone <- tRead . toUpper <$> pCharSet "abcdefg"
           sharp <- pOptional (pCharSet "#")
           if isJust sharp && tone `elem` [C,D,F,G,A]
             then return (succ tone)
             else return tone
  where tRead 'C' = C
        tRead 'D' = D
        tRead 'E' = E
        tRead 'F' = F
        tRead 'G' = G
        tRead 'A' = A
        tRead 'B' = B
        tRead _   = error "Invalid note"

-- TODO: Schrijf en documenteer de Parser` `pOctave`, die een getal parset naar de bijbehorende octaaf.
-- TIPS: Kijk in Types.hs naar het type Octave voor je begint te schrijven.
{-|
zet getallen om naar het corrosponderende Octave data type 
maakt van een parse van alle gehele getallen en geven deze mee als input
als een input in de lijst, we geven elk meegegeven integer met de where keyword het corrosponderende octave dat type
en als alle integers zijn omgezet geeft de code de types terug. 
-}
pOctave :: Parser Octave
pOctave = do input <- tRead <$> pCharSet "012345678"
             return input
    where tRead '0' = Zero
          tRead '1' = One
          tRead '2' = Two
          tRead '3' = Three
          tRead '4' = Four
          tRead '5' = Five
          tRead '6' = Six
          tRead '7' = Seven
          tRead '8' = Eight

pDuration :: Parser Duration
pDuration = do number <- pNumber
               case number of
                 1 -> return Full
                 2 -> return Half
                 4 -> return Quarter
                 8 -> return Eighth
                 16 -> return Sixteenth
                 32 -> return Thirtysecond
                 _ -> mzero

pPause :: Duration -> Parser Note
pPause d = do duration <- fromMaybe d <$> pOptional pDuration
              _ <- pCharSet "pP"
              return $ Pause duration

pNote :: Duration -> Octave -> Parser Note
pNote d o = do duration <- fromMaybe d <$> pOptional pDuration
               tone <- pTone
               dot <- pOptional (pCharSet ".")
               octave <- fromMaybe o <$> pOptional pOctave
               return $ Note tone octave (if isJust dot then Dotted duration else duration)

pComma :: Parser ()
pComma = () <$ do _ <- pCharSet ","
                  pOptional (pCharSet " ")

-- TODO: Schrijf en documenteer de Parser `pHeader`, die de start van de RTTL-string parset.
-- TIPS: We hebben je de naam van het bestand, en het converteren van bpm met fromIntegral vast gegeven.
--       Het stuk dat je rest om te parsen zit tussen de twee dubbele punten!
{-|
parset een RTTL-string
pakt de RTTL-string en loopt karakter voor karater door de RTTL-String en parset elke Char die de code tegenkomt
als de code bij een char of combinatie van chars komt die we willen terug krijgen geeft de code deze een waarde mee. 
als de gehele RTTL-string is geparset worden de chars die een naam hebben gekregen in de return statemnt gestopt en terug gegeven 
-}
pHeader :: Parser (String, Duration, Octave, Beats)
pHeader = do name <- pRepeat (pComplementCharSet ":")
             _ <- pCharSet ":"
             _ <- pCharSet "d"
             _ <- pCharSet "="
             dur <- pDuration
             _ <- pComma
             _ <- pCharSet "o"
             _ <- pCharSet "="
             octa <- pOctave
             _ <- pComma
             _ <- pCharSet "b"
             _ <- pCharSet "="
             bpm <- pNumber
             _ <- pCharSet ":"
             return (name, dur, octa, fromIntegral bpm) -- Pas deze regel ook aan; maak van 0 de waarde van bpm!

pSeparator :: Parser ()
pSeparator = () <$ foldl1 mplus [pString " ", pString ", ", pString ","]

pRTTL :: Parser (String, [Note], Beats)
pRTTL = do (t, d, o, b) <- pHeader
           notes <- pRepeatSepBy pSeparator $ mplus (pNote d o) (pPause d)
           return (t, notes, b)

parse :: String -> Maybe (String, [Note], Beats)
parse x = evalStateT pRTTL x