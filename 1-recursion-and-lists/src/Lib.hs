{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: recursie en lijsten
    Copyright   : (c) Brian van de Bijl, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum oefenen we met het schrijven van simpele functies in Haskell.
    Specifiek leren we hoe je recursie en pattern matching kunt gebruiken om een functie op te bouwen.
    LET OP: Hoewel al deze functies makkelijker kunnen worden geschreven met hogere-orde functies,
    is het hier nog niet de bedoeling om die te gebruiken.
    Hogere-orde functies behandelen we verderop in het vak; voor alle volgende practica mag je deze
    wel gebruiken.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- TODO: Schrijf en documenteer de functie ex1, die de som van een lijst getallen berekent.
-- Voorbeeld: ex1 [3,1,4,1,5] ~> 14
{-| 
berekent het totaal van een lijst. 
pakt het eerste element van xs en tel die op bij het totaal 
als de lijst leeg is stop de functie  
-}
ex1 :: [Int] -> Int
ex1 (x:xs) = x + ex1 xs
ex1 [] = 0

-- TODO: Schrijf en documenteer de functie ex2, die alle elementen van een lijst met 1 ophoogt.
-- Voorbeeld: ex2 [3,1,4,1,5] ~> [4,2,5,2,6]
{-| 
telt 1 op bij ieder element van de lijst. 
pakt het eerst volgende element van xs en telt er 1 bij  
als de code door de hele lijst heen is stopt de code 
-}
ex2 :: [Int] -> [Int]
ex2 (x:xs) = (x + 1) : ex2 xs
ex2 [] = []  

-- TODO: Schrijf en documenteer de functie ex3, die alle elementen van een lijst met -1 vermenigvuldigt.
-- Voorbeeld: ex3 [3,1,4,1,5] ~> [-3,-1,-4,-1,-5]
{-| 
vermingvuldigd ieder element in de lijst met -1. 
pakt het eerst volgende element van xs en doet die keer -1 
als de code door de hele lijst heen is stopt de code 
-}
ex3 :: [Int] -> [Int]
ex3 (x:xs) = (x * (-1)) : ex3 xs 
ex3 [] = []

-- TODO: Schrijf en documenteer de functie ex4, die twee lijsten aan elkaar plakt.
-- Voorbeeld: ex4 [3,1,4] [1,5] ~> [3,1,4,1,5]
-- Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. 
-- Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
{-| 
combineert twee lijsten samen in één lijst
deffineer ex4 als element voor element als de eerste lijst 
als ex4 gelijk is aan de eerste lijst, voeg de tweede lijst in het einde van de eerste lijst 
-}
ex4 :: [Int] -> [Int] -> [Int]
ex4 (x:xs) ys = x : ex4 xs ys 
ex4 [] ys = ys

-- TODO: Schrijf en documenteer een functie, ex5, die twee lijsten van gelijke lengte paarsgewijs bij elkaar optelt.
-- Voorbeeld: ex5 [3,1,4] [1,5,9] ~> [4,6,13]
{-| 
telt twee gelijke lengte lijsten bij elkaar op. 
pakt de eerst volgende elementen van beide lijsten op corresponderende index 
tel de twee gepakte elementen bij elkaar op en stop deze in een nieuwe lijst
stop de code als we door de lijsten heen zijn  
-}
ex5 :: [Int] -> [Int] -> [Int]
ex5 (x:xs) (y:xy) = (x + y) : ex5 xs xy
ex5 [] [] = []

-- TODO: Schrijf en documenteer een functie, ex6, die twee lijsten van gelijke lengte paarsgewijs met elkaar vermenigvuldigt.
-- Voorbeeld: ex6 [3,1,4] [1,5,9] ~> [3,5,36] 
{-| 
vermeningvuldig twee gelijke lengte lijsten bij elkaar op. 
pakt de eerst volgende elementen van beide lijsten op corresponderende index 
vermeningvuldig de twee gepakte elementen met elkaar en stop deze in een nieuwe lijst
stop de code als we door de lijsten heen zijn  
-}
ex6 :: [Int] -> [Int] -> [Int]
ex6 (x:xs) (y:xy) = (x * y) : ex6 xs xy
ex6 [] [] = []
-- TODO: Schrijf en documenteer een functie, ex7, die de functies ex1 en ex6 combineert tot een functie die het inwendig product uitrekent.
-- Voorbeeld: ex7 [3,1,4] [1,5,9] geeft 3*1 + 1*5 + 4*9 = 44 terug als resultaat.
{-| 
berkent het totaal van het vermeningvuldigen van twee gelijke lengte lijsten met elkaar. 
pakt de eerst volgende elementen van beide lijsten op corresponderende index 
tel de twee gepakte elementen bij elkaar op en tel het berekende getal op bij het totaal
stop de code als we door de lijsten heen zijn  
-}
ex7 :: [Int] -> [Int] -> Int
ex7 (x:xs) (y:xy) = (x * y) + ex7 xs xy 
ex7 [] [] = 0
